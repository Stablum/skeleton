Skeleton project for web app
============================

## Introduction

Skeleton is a project template for a complete web app written in python
and very specific technologies.
It provides all configuration files and basic first implementation.

## Technologies employed

The technologies that are used for this template project are:

* flask
* flask_restful
* flask_sqlalchemy
* bower
* alembic
* deploytools
* nginx
* PostGreSQL

## Features of the web app

The web app is separated in two component apps: web.py and api.py

**api.py** offers a RESTful API that can act as a _model/controller_ component. 
It retrieves and stores data to the PostGreSQL, which is meant to be
completely isolated from the web interface. 

**web.py** offers a web interface that can act as a _controller/view_ component.
It stores and retrieves data with the RESTful API.

## Basic setup

Change _config/api.json_'s SQLALCHEMY_DATABASE_URI to point to a PostGreSQL database.

Change hostnames in _deploy.yaml_

