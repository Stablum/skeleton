from flask import Flask
import flask_restful
import flask_sqlalchemy
import services
import model
from model import Base
from model.story import Story

from model import story

app = Flask(__name__)
app.config.from_json('config/api.json')
api = flask_restful.Api(app)
db = flask_sqlalchemy.SQLAlchemy(app)

api.add_resource(services.story.Story, '/story/','/story/<int:uuid>')


Base.metadata.create_all(db.engine)

print('api app, all done.')