from flask import Flask
from flask import render_template

app = Flask(__name__)
app.config.from_json('config/web.json')

@app.route('/')
def home():
    return render_template("home.html")

